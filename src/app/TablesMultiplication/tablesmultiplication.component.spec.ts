import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesmultiplicationComponent } from './tablesmultiplication.component';

describe('TablesmultiplicationComponent', () => {
  let component: TablesmultiplicationComponent;
  let fixture: ComponentFixture<TablesmultiplicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablesmultiplicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesmultiplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
