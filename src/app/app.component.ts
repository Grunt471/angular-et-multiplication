import { Component, Input, EventEmitter, SystemJsNgModuleLoader } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  chiffre=0;
  

  title = 'multiplication';
  identForm!: FormGroup;
  submitted = false;
  badChiffre = false;
  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      chiffre: new FormControl(''),
      });     
  }
  get formControls() { return this.identForm.controls; }

  submit() {
    this.submitted = true;
    this.chiffre = this.identForm.get('chiffre')?.value;
    let tab = [1,2,3,4,5,6,7,8,9,10];
    for ( var i = 0 ; i < tab.length ; i++) {
        let calcul = tab[i]*this.chiffre;
      console.log(this.chiffre + " X " + tab[i] + " = " + calcul);


  }
  console.table(tab);

}
}


